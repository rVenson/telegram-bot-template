# Bot PHP Template for Telegram

A [Telegram Bot](https://core.telegram.org/bots/api) template.

## Instalation

To run this bot in production, you will need tree configurations:

1. Create the Telegram Bot at @Bot_Father.
2. Update define ``BOT_TOKEN``, ``API_URL`` and ``WEBHOOK_URL``.
3. Implement the logic in ``processMessage`` function.
4. Update the webhook url.

### Create telegram bot
To create a telegram bot on Telegram Bot API, see the [offical documentation](https://core.telegram.org/bots/api).

### Configuring webhook
This bot works on webhook mode. You need to update the webhook url at telegram bot api. This can be done with the following command:

````bash
curl -F "url=URL" https://api.telegram.org/botTELEGRAM_TOKEN/setWebhook
````
> Replace URL with the full public path to your gamescorerobot.php
> Replace TELEGRAM_TOKEN with your telegram bot token

Or executing the file ``template.php`` on console with php, after define the ``WEBHOOK_URL``:

````
php template.php
````